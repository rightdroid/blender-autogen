import bpy
import random
import re
import math

master_seeds = 10
child_generations = 6
connection_radius = 0.2


class Main():
    def __init__(self):
        self.bbox = None
        self.bbox_corner_lower = [0,0,0]
        self.bbox_corner_upper = [0,0,0]

        # self.cubegen_scale_lower = [.0,.0,.0]
        # self.cubegen_scale_upper = [1.5,1.5,1.5]
        self.cubegen_seed_count = master_seeds
        self.cubegen_cube_minsize = 0.2
        self.cubegen_cube_maxsize = 1.3


        # run main script
        if 'Bounding Box' in bpy.data.objects.keys():
            self.run()
        else:
            print('Bounding box not found. The box must be named "Bounding Box"' + "\n")


    def run(self):
        #remove all previous autogen_ objects
        self.remove_previous()

        # create group for connectionMeshes
        bpy.ops.group.create(name="autogen_connectionGroup")

        # get bbox object
        self.bbox = bpy.data.objects['Bounding Box']

        self.get_bbox_bounds_coords()

        self.generate_seed_cubes()

    def remove_previous(self):

        # remove group if exists
        if 'autogen_connectionGroup' in bpy.data.groups:
            bpy.data.groups.remove(bpy.data.groups['autogen_connectionGroup'], do_unlink=True)

        # deselect all
        bpy.ops.object.select_all(action='DESELECT')

        # find objects
        r = re.compile("autogen_.*")
        result = filter(r.match, bpy.data.objects.keys())
        for res in result:
            # selection
            bpy.data.objects[res].select = True

            # remove it
            bpy.ops.object.delete()

    def get_bbox_bounds_coords(self):
        loc = self.bbox.location
        scale = self.bbox.scale
        self.bbox_corner_lower[0] = loc[0] - scale[0]
        self.bbox_corner_lower[1] = loc[1] - scale[1]
        self.bbox_corner_lower[2] = loc[2] - scale[2]

        self.bbox_corner_upper[0] = loc[0] + scale[0]
        self.bbox_corner_upper[1] = loc[1] + scale[1]
        self.bbox_corner_upper[2] = loc[2] + scale[2]
        print('min %s' % self.bbox_corner_lower)
        print('max %s' % self.bbox_corner_upper)


    def generate_seed_cubes(self):

        # generate seed cubes
        for x in range(0,self.cubegen_seed_count):
            random_radius = random.uniform(self.cubegen_cube_minsize, self.cubegen_cube_maxsize)
            random_location = [0,0,0]
            random_rotation = [0,0,0]
            random_size = [0,0,0]
            random_name_float = 0.0

            # generate locations, while ensuring we are in bounding box
            shift_from_edge = round(random_radius * 2.3,4)
            # print('obj: %s shift: %s' % (x, shift_from_edge))
            for a in range(0,3):
                random_location[a] = random.uniform(self.bbox_corner_lower[a]+shift_from_edge, self.bbox_corner_upper[a]-shift_from_edge)
                random_rotation[a] = random.uniform(-math.pi, math.pi)
            random_size[0] = random_size[1] = random_size[2] = random_radius



            if(x == 0):
                bpy.ops.mesh.primitive_cube_add(radius=random_radius, location=random_location, rotation=random_rotation)


            else:
                bpy.ops.object.duplicate_move_linked(OBJECT_OT_duplicate={"linked":True, "mode":'TRANSLATION'}, TRANSFORM_OT_translate={"value":(0,0,0)})
                ob = bpy.context.object
                ob.location = random_location
                ob.scale = random_size
                ob.rotation_euler = random_rotation

            ob = bpy.context.object
            me = ob.data
            ob.name = 'autogen_cube_' + str(x+1)
            me.name = 'autogen_cube_mesh_' + str(x+1)

            seed_master = MasterSeed(ob.name, random_location, random_size, random_rotation)



class BaseOperators():
    def __init__(self):
        self.connection_radius = connection_radius

    def createMesh(self, name, origin, verts, edges, faces):

        mesh_data = bpy.data.meshes.new(name+'Mesh')
        mesh_data.from_pydata(verts, [], faces)
        mesh_data.update()

        obj = bpy.data.objects.new(name, mesh_data)

        scene = bpy.context.scene
        scene.objects.link(obj)
        # obj.select = True
        return obj

    def generate_random_location(self, old_location, distance_variation):
        new_location = [self.parent.location[0],self.parent.location[1],self.parent.location[2]]
        location_shift = (self.parent.scale[0]) + distance_variation

        for x in range(0,3):
            rand_bool = random.randint(1,151)

            if rand_bool < 50:
                new_location[x] = self.parent.location[x] + location_shift
            elif rand_bool > 50 and rand_bool < 100:
                new_location[x] = self.parent.location[x] - location_shift
            else:
                new_location[x] = self.parent.location[x]

        return new_location

class MasterSeed(BaseOperators):
    def __init__(self, name, location, scale, rotation):
        super(MasterSeed, self).__init__()

        self.gen_cap = child_generations
        self.gen_spawned = 0

        self.name = name
        self.location = location
        self.scale = scale
        self.rotation = rotation



        child = ChildSeed(self, self)

class ChildSeed(BaseOperators):
    def __init__(self, master, parent):
        super(ChildSeed, self).__init__()

        self.parent = parent
        self.master = master
        self.master.gen_spawned += 1
        self.scale_modifier = 1
        # self.scale_modifier = random.uniform(0.8, 1.2)


        self.location = None
        self.scale = None
        self.rotation = None

        # create object
        bpy.ops.object.duplicate_move_linked(OBJECT_OT_duplicate={"linked":True, "mode":'TRANSLATION'}, TRANSFORM_OT_translate={"value":(0,0,0)})
        self.object = bpy.context.object
        self.name = self.object.name

        self.set_scale_and_rotation()
        self.set_location()


        # create a mesh connecting this seed to its child
        (x0,y0,z0) = self.parent.location
        (x1,y1,z1) = self.location
        con_r = self.connection_radius
        verts = ((x0,y0,z0-con_r), (x0,y0-con_r,z0), (x0-con_r,y0,z0), (x1,y1,z1-con_r), (x1,y1-con_r,z1), (x1-con_r,y1,z1))
        faces = ((1,0,2),(0,3,5,2),(0,1,4,3),(2,5,4,1),(4,5,3))
        self.con_obj = self.createMesh('autogen_connection', (x0,y0,z0), verts, [], faces)


        bpy.data.objects[self.con_obj.name].select = True
        bpy.context.scene.objects.active = bpy.data.objects[self.con_obj.name]
        bpy.ops.object.group_link(group="autogen_connectionGroup")

        bpy.data.objects[self.con_obj.name].select = False
        bpy.context.scene.objects.active = bpy.data.objects[self.name]
        # bpy.ops.object.select_all(action='DESELECT')
        # if hasattr(self.parent, 'con_obj'):
            # bpy.data.objects[self.con_obj.name].select = True
        #     bpy.data.objects[self.parent.con_obj.name].select = True
        #     bpy.context.scene.objects.active = bpy.data.objects[self.con_obj.name]
        #     bpy.ops.object.make_links_data(type='OBDATA')
        #     bpy.ops.object.select_all(action='DESELECT')



        self.spawn_new_child()


    def set_scale_and_rotation(self):
        # --- generate and set scale and rotation ---
        self.scale = self.object.scale = (self.parent.scale[0]*self.scale_modifier,self.parent.scale[1]*self.scale_modifier,self.parent.scale[2]*self.scale_modifier)
        self.rotation = self.object.rotation_euler = self.parent.rotation

    def set_location(self):
        # --- generate and set new location ---
        new_location = self.generate_random_location(self.parent.location, self.parent.scale[0] * 1.5)
        while new_location == self.parent.location:
            new_location = self.generate_random_location(self.parent.location, self.parent.scale[0] * 1.5)
        self.location = self.object.location = new_location


    def spawn_new_child(self):
        # --- repeat spawning until generation cap is reached ---
        if self.master.gen_spawned < self.master.gen_cap:
            child = ChildSeed(self.master, self)





Main()